package socool;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.imageio.ImageIO;


public class Grabber {
	private static final String PKG_TAG = " data-docid";
	private static final String IMAGE_TAG = "cover-image";
	private static final String SEARCH_URL = "https://play.google.com/store/search?q=";
	private static final String APP_URL = "https://play.google.com/store/apps/details?id=";
	
	public String searchByTerm(String search) throws IOException {
		String fullSearch = SEARCH_URL + URLEncoder.encode(search, "UTF-8");;
		System.out.println("Searching: " + fullSearch);
		
		URL searchUrl = null;
		searchUrl = new URL(fullSearch);
		
        BufferedReader in = null;
		in = new BufferedReader(
		new InputStreamReader(searchUrl.openStream()));

        String inputLine;
        String pkg = null;
        while ((inputLine = in.readLine()) != null) {
            //System.out.println(inputLine);
            if (inputLine.contains(PKG_TAG)) {
			int startIndex = inputLine.indexOf("\"", inputLine.indexOf(PKG_TAG));
			int endIndex = inputLine.indexOf("\"", startIndex+1);
			pkg = inputLine.substring(startIndex+1, endIndex);
			System.out.println("Pkg: " + pkg);
            	break;
            }
        }
        in.close();
        return pkg;
	}
	
	public boolean saveLauncherImageBasedOnPackageName(String packageName) throws IOException {
		String url = APP_URL + packageName;
		URL myURL;
		String myImageURL = null;
		BufferedReader in = null;
		System.out.println("url: " + url );
		myURL = new URL(url);
		
		in = new BufferedReader(new InputStreamReader(myURL.openStream()));
		String inputLine;
		//Scan through all of the HTML and find the right things we want
		while ((inputLine = in.readLine()) != null) {
			if (inputLine.contains(IMAGE_TAG)) {
				int index;
				int index2;
				index = inputLine.indexOf(IMAGE_TAG);
				index = index + IMAGE_TAG.length() + 2;
				index2 = inputLine.indexOf("alt", index);
				index2 = index2 - 1;
				myImageURL = inputLine.substring(index, index2);
				myImageURL = myImageURL.substring(myImageURL.indexOf("\"")+1);
				myImageURL = myImageURL.substring(0, myImageURL.indexOf("\""));
				System.out.println("Downloading: " + myImageURL + "...");
				break;
			}
			
		}
		in.close();
		BufferedImage image = null;
		URL imgUrl;
		try {
			imgUrl = new URL(myImageURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return false;
		}
		
		try {
			image = ImageIO.read(imgUrl);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		 
        try {
			ImageIO.write(image, "png",new File(packageName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
        
        //Made it here. Yay!
        return true;
	}
	

}
