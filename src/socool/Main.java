package socool;
import java.io.IOException;
import java.util.Scanner;


public class Main {

	
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Grabber grabber = new Grabber();
		Scanner in = new Scanner(System.in);
		String input;
		boolean looper = true;
		while (looper) {
			System.out.println("Enter a string");
			input = in.nextLine();
			String searchResult = grabber.searchByTerm(input);
			if (searchResult != null) {
				if (grabber.saveLauncherImageBasedOnPackageName(searchResult)) {
					System.out.println("File " + searchResult
							+ " saved successfully");
				}
			}
		}
		
		in.close();
	}

}
