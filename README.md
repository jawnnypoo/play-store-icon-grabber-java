# README #

This is a simple Java project that I made for the purpose of getting high resolution icons in order for designers to translate them into new icons for an icon pack. Icons should pull in 512x512.  
  
** To Use: **  
The library has been compiled into a jar to make things easier  

**Windows**  
1. Navigate to CheckItOut folder.   
2. Run the PlayIcon.bat  
3. Search for icons within the command line  
4. If icons are not showing up by key term, try searching the package name  
  
**Unix/Linux/Mac**  
1. Open up a terminal session  
2. Run the command "java -jar lib/grabber.jar"  
3. Enjoy!